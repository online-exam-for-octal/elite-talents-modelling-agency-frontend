import React, { useState } from "react";
import ErrorMessage from "./ErrorMessage";

const AuditionForm = ({ url }) => {
  const [error, setError] = useState({
    exists: false,
    message: null,
  });

  const [userInfo, setUserInfo] = useState({
    firstname: null,
    lastname: null,
    email: null,
    imageOne: null,
  });

  const [secondImage, setSecondImage] = useState({
    imageTwo: null,
  });

  const [imageToBeSavedOne, setImageToBeSavedOne] = useState({
    file: "https://dummyimage.com/150/c5c5c5/fff.png&text=img",
  });

  const [imageToBeSavedTwo, setImageToBeSavedTwo] = useState({
    file: "https://dummyimage.com/150/c5c5c5/fff.png&text=img",
  });

  const handleFileUploadOne = (e) => {
    setUserInfo({
      ...userInfo,
      imageOne: e.target.files[0],
    });

    if (e.target.files[0]) {
      setImageToBeSavedOne({
        file: URL.createObjectURL(e.target.files[0]),
      });
    }

    console.log(userInfo);
  };

  const handleFileUploadTwo = (e) => {
    setSecondImage({
      imageTwo: e.target.files[0],
    });

    if (e.target.files[0]) {
      setImageToBeSavedTwo({
        file: URL.createObjectURL(e.target.files[0]),
      });
    }

    console.log(userInfo);
  };

  const handleChange = (e) => {
    setUserInfo({
      ...userInfo,
      [e.target.name]: e.target.value,
    });
  };

  const handleCheck = (e) => {
    setUserInfo({
      ...userInfo,
      isChecked: !userInfo.isChecked,
    });
  };

  const handleAudition = (e) => {
    e.preventDefault();

    setUserInfo({
      ...userInfo,
      images: [userInfo.imageOne, userInfo.imageTwo],
    });

    let formData = new FormData();
    formData.append("imageOne", userInfo.imageOne);
    formData.append("firstname", userInfo.firstname);
    formData.append("lastname", userInfo.lastname);
    formData.append("email", userInfo.email);
    formData.append("isChecked", userInfo.isChecked);
    let formDataTwo = new FormData();
    formDataTwo.append("imageTwo", secondImage.imageTwo);
    console.log(userInfo.isChecked);

    fetch(url + "/talents/audition", {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        if (data.error) {
          setError({
            exists: true,
            action: "Login",
            message: data.error,
          });
        } else {
          console.log(data._id);
          setError({
            exists: false,
            message: false,
          });
          fetch(url + "/talents/" + data._id, {
            method: "PUT",
            body: formDataTwo,
          })
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              if (data.error) {
                setError({
                  exists: true,
                  action: "Login",
                  message: data.error,
                });
              } else {
                console.log(data);
                setError({
                  exists: false,
                  message: false,
                });
              }
            });
        }
      });
  };

  return (
    <React.Fragment>
      <div className="col-12">
        {error.message && <ErrorMessage error={error} />}
      </div>
      <div className="col-6 mx-auto my-5">
        <div className="col-12">
          <form onSubmit={handleAudition}>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">First name</label>
              <input
                name="firstname"
                type="text"
                className="form-control"
                onChange={handleChange}
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Last name</label>
              <input
                name="lastname"
                type="text"
                className="form-control"
                onChange={handleChange}
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Email address</label>
              <input
                name="email"
                type="email"
                className="form-control"
                onChange={handleChange}
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>

            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Images</label>
              <input
                name="firstname"
                type="file"
                className="form-control"
                onChange={handleFileUploadOne}
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>

            <div className="form-group">
              <input
                name="firstname"
                type="file"
                className="form-control"
                onChange={handleFileUploadTwo}
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>

            <img
              className="mr-2 image-display"
              src={imageToBeSavedOne.file}
              alt=""
            />
            <img
              className="mr-2 image-display"
              src={imageToBeSavedTwo.file}
              alt=""
            />

            <div className="form-group form-check">
              <input
                onChange={handleCheck}
                type="checkbox"
                className="form-check-input"
                id="exampleCheck1"
              />
              <label className="form-check-label" htmlFor="exampleCheck1">
                I agree to Elite Talents site's terms and conditions.
              </label>
            </div>

            <button type="submit" className="btn btn-primary">
              Audition as Model
            </button>
          </form>
        </div>
      </div>
    </React.Fragment>
  );
};

export default AuditionForm;
