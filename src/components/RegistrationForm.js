import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import ErrorMessage from "./ErrorMessage";

const RegistrationForm = ({ url, handleLoginSuccess }) => {
  const [error, setError] = useState({
    exists: false,
    message: null,
  });

  const history = useHistory();

  const [userInfo, setUserInfo] = useState({
    firstname: null,
    lastname: null,
    email: null,
    password: null,
    confirmPassword: null,
    isChecked: false,
  });

  const handleChange = (e) => {
    setUserInfo({
      ...userInfo,
      [e.target.name]: e.target.value,
    });
  };

  const handleCheck = (e) => {
    setUserInfo({
      ...userInfo,
      isChecked: !userInfo.isChecked,
    });
  };

  const handleRegistration = (e) => {
    e.preventDefault();
    fetch(url + "/users/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userInfo),
    })
      .then((response) => {
        return response.json();
      })
      .then((user) => {
        if (user._id) {
          setError({
            exists: false,
            message: null,
          });
          fetch(url + "/users/login", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: userInfo.email,
              password: userInfo.password,
            }),
          })
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              if (data.error) {
                setError({
                  exists: true,
                  action: "Login",
                  message: data.error,
                });
              } else {
                console.log(data);
                setError({
                  exists: false,
                  message: false,
                });
                //   setSuccess({
                //     exists: true,
                //     action: "Login",
                //   });
                window.localStorage.setItem("token", "Bearer " + data.token);
                history.push("/");
                handleLoginSuccess(data.user);
                setUserInfo({
                  firstname: null,
                  lastname: null,
                  email: null,
                  password: null,
                  confirmPassword: null,
                  isChecked: false,
                });
              }
            });
        } else {
          setError({
            error: true,
            message: user.error,
          });
        }
      });
  };

  return (
    <React.Fragment>
      <div className="col-12">
        {error.message && <ErrorMessage error={error} />}
      </div>
      <div className="col-12">
        <form onSubmit={handleRegistration}>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">First name</label>
            <input
              name="firstname"
              type="text"
              className="form-control"
              onChange={handleChange}
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Last name</label>
            <input
              name="lastname"
              type="text"
              className="form-control"
              onChange={handleChange}
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Email address</label>
            <input
              name="email"
              type="email"
              className="form-control"
              onChange={handleChange}
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Password</label>
            <input
              name="password"
              type="password"
              className="form-control"
              onChange={handleChange}
              id="exampleInputPassword1"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1"> Confirm Password</label>
            <input
              name="confirmPassword"
              type="password"
              className="form-control"
              onChange={handleChange}
              id="exampleInputPassword1"
            />
          </div>
          <div className="form-group form-check">
            <input
              onChange={handleCheck}
              type="checkbox"
              className="form-check-input"
              id="exampleCheck1"
            />
            <label className="form-check-label" htmlFor="exampleCheck1">
              I agree to Elite Talents site's terms and conditions.
            </label>
          </div>
          <button type="submit" className="btn btn-primary">
            Register Now
          </button>
        </form>
      </div>
    </React.Fragment>
  );
};

export default RegistrationForm;
