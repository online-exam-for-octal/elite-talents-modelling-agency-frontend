import React from "react";
import { NavLink } from "react-router-dom";

const Footer = () => {
  return (
    <div className="container-fluid footer py-5 mx-0">
      <div className="row d-flex justify-content-center my-3">
        <div className="col-sm-12 col-lg-1 col-md-12 text-center">
          <h6 className="mx-1">
            <NavLink className="footer-link" to="/">
              Home
            </NavLink>
          </h6>
        </div>
        <div className="col-sm-12 col-lg-1 col-md-12 text-center">
          <h6 className="mx-1">
            <NavLink className="footer-link" to="/audition">
              Audition Now
            </NavLink>
          </h6>
        </div>
        <div className="col-sm-12 col-lg-1 col-md-12 text-center">
          <h6 className="mx-1">
            <NavLink className="footer-link" to="/about">
              About Us
            </NavLink>
          </h6>
        </div>
        <div className="col-sm-12 col-lg-1 col-md-12 text-center">
          <h6 className="mx-1">
            <NavLink className="footer-link" to="/contact">
              Contact Us
            </NavLink>
          </h6>
        </div>
      </div>
      <div className="col-12 px-0 text-center">
        <p className="mb-0">
          Use of this web site constitutes acceptance of the Privacy Policy and
          Terms of Service.
        </p>
      </div>
      <div className="col-12 px-0 text-center my-3">
        <p className="mb-0">&copy; 2020 RC Ferrer. All rights reserved</p>

        <p className="mb-0">
          Elite Talent Bldg., Ronquillo St., Sta. Cruz, Manila, Philippines 1559
        </p>
      </div>
    </div>
  );
};

export default Footer;
