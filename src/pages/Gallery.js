import React from "react";

const Gallery = () => {
  return (
    <div>
      <div className="container my-5">
        <div className="row">
          <div className="col-12 d-flex flex-wrap">
            <div className="col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F1171558146%2F960x0.jpg%3Ffit%3Dscale&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Runway Contest, PH 2018
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.hawtcelebs.com%2Fwp-content%2Fuploads%2F2018%2F02%2Fgigi-hadid-at-versace-runway-show-at-milan-fashion-week-02-23-2018-16.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    ACF Fashion Show, Singapore 2019
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.theskinnybeep.com%2Fwp-content%2Fuploads%2F2016%2F01%2FCalvin-Klein-Spring-Summer-2016-Womenswear-Collection-London-Fashion-Week-Fashion-Show.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Fashion Show, PH 2019
                  </p>
                </div>
              </div>
            </div>

            <div className="my-3 col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fsympli.com%2Fwp-content%2Fuploads%2F2017%2F08%2Fsympli_evolutionofmodels_beverlyjohnson.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Fashion Show, PH 2019
                  </p>
                </div>
              </div>
            </div>
            <div className="my-3 col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmodelsbyanilblon.files.wordpress.com%2F2015%2F04%2Fcameron-russell-american-fashion-model-3.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Fashion Show, PH 2019
                  </p>
                </div>
              </div>
            </div>
            <div className="my-3 col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.hawtcelebs.com%2Fwp-content%2Fuploads%2F2019%2F01%2Fbella-hadid-at-versace-runway-show-at-milan-fashion-week-01-12-2019-3.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Fashion Show, PH 2019
                  </p>
                </div>
              </div>
            </div>
            <div className="my-3 col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fblog.lenismodelmanagement.co.uk%2Fwp-content%2Fuploads%2F2015%2F09%2Fedit-6.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Fashion Show, PH 2019
                  </p>
                </div>
              </div>
            </div>
            <div className="my-3 col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ffashionunited.uk%2Fimages%2F201812%2FxAlexander-McQuee6.jpg.pagespeed.ic.lvO1-S2z77.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Fashion Show, PH 2019
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Gallery;
