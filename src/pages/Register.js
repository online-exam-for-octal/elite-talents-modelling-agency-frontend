import React from "react";
import RegistrationForm from "../components/RegistrationForm";

const Register = ({ url, handleLoginSuccess }) => {
  return (
    <div className="container my-5">
      <div className="row">
        <div className="col-12">
          <h1 className="text-center">Register</h1>
        </div>
        <RegistrationForm url={url} handleLoginSuccess={handleLoginSuccess} />
      </div>
    </div>
  );
};

export default Register;
